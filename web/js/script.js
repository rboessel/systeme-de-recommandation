$(document).ready(function () {
    var index = 0;
    $('#add').click(function (e) {
        e.preventDefault();
        index++;
        $(".reponses").append('<div class="form-group reponse">'
                + '<label>R&eacute;ponse</label>'
                + '<div class="input-group">'
                + '<input type="text" value="" class="form-control" name="reponses['
                + index + ']" id="reponses' + index + '" >'
                + '<span class="input-group-addon" >'
                + '<input type="radio" value="' + index + '" name="index" id="index'
                + (index + 1) + '" > </span></div>'
                );

    });
    $('#del').click(function (e) {
        e.preventDefault();
        if (index > 0) {
            $(".reponse").last().remove();
            index--;
        }
    });
    var destroy = $('#destroy .destroy');
    destroy.click(function (e) {
        e.preventDefault();
        var r = confirm("Voulez vous vraiment supprimer cette question ?");
        if (r === true) {
            window.location.href = destroy.attr('href');
        }
    });
});
