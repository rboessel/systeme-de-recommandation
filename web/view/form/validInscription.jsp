<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="validation"/>
</jsp:include>
<h2>Inscription</h2>
<div class="row">
    <div class="col-lg-6">
        <div class="alert alert-success" role="alert">
            <span class="glyphicon glyphicon-valid" aria-hidden="true"></span>
            Utilisateur ajouté avec succès !
        </div>
        <a href="./connexion">Vous connecter</a>
    </div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>
