<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="inscription"/>
</jsp:include>
<h2>Inscription</h2>
<div class="row">
    <div class="col-lg-6">
        <form:form modelAttribute="inscription" method="POST" action="./inscription">
            <c:set var="genericError"><form:errors path="*"/></c:set>
            <c:if test="${not empty genericError}">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    ${genericError}
                </div>
            </c:if>
            <div class="form-group">
                <label for="name">Entrez votre Nom</label>
                <div class="input-group">
                    <form:input id="name" type="text" path="name" cssClass="form-control" placeholder="Entrez votre nom" required=""/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            <div class="form-group">
                <label for="email">Entrez votre E-mail</label>
                <div class="input-group">
                    <form:input path="email" type="email" cssClass="form-control" placeholder="Entrez votre email" required=""/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            <div class="form-group">
                <label for="password">Entrer votre mot de passe</label>
                <div class="input-group">
                    <form:input type="password" path="password" cssClass="form-control" required=""/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            <input type="submit" name="submit" id="submit" value="envoyer" class="btn btn-info pull-right">
        </form:form>   
        <a href="./connexion" >Vous avez déjà un compte ?</a>       
    </div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>
