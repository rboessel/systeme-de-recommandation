<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="accueil"/>
</jsp:include>
<h2>Connexion</h2>
<div class="row">
    <div class="col-lg-6">
        <form:form modelAttribute="connexion" method="POST" action="./connexion">
            <c:set var="genericError"><form:errors path="*"/></c:set>
            <c:if test="${not empty genericError}">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    ${genericError}
                </div>
            </c:if>
            <div class="form-group">
               <label for="name">Entrez votre Nom</label>
                <div class="input-group">
                    <form:input id="name" path="name" cssClass="form-control" placeholder="Entrez votre nom" required=""/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            <div class="form-group">
                <label for="password">Entrer votre mot de passe</label>
                <div class="input-group">
                    <form:input id="password" path="password" type="password" cssClass="form-control" placeholder="Entrez votre mot de passe" required=""/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            <input type="submit" name="submit" id="submit" value="envoyer" class="btn btn-info pull-right">
        </form:form>
        <a href="./inscription" >Vous n'avez pas de compte ?</a>
    </div>
</div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>

