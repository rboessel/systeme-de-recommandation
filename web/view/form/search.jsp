<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<form:form class="jumbotron" modelAttribute="search" method="POST" action="${pageContext.request.contextPath}/recherche">
    <div class="row">
        <div class="col-xs-9">
            <form:input path="query" cssClass="form-control" />
        </div>
        <input  class="btn btn-default col-xs-2" value="rechercher" type="submit"/>
    </div>      
    <div class="row">
        <c:set var="genericError">${error}</c:set>
        <c:if test="${not empty genericError}">
            <div class="col-xs-4">
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    ${genericError}
                </div>
            </div>
        </c:if>
    </div>
</form:form>