<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Systeme de Reccommandation - ${param.title}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css"  href="${pageContext.request.contextPath}/css/style.css"/>
        <style>body{padding-top: 70px;}</style>
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${pageContext.request.contextPath}/recherche">Moteur de recherche</A>
                    <c:if test="${sessionScope.userSession != 'defaultUser'}">
                        <div style="display:inline-block;" class="navbar-text inline-block">
                            login: <strong>${sessionScope.userSession}</strong>
                        </div>
                    </c:if>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse" >
                    <c:if test="${sessionScope.userSession == 'defaultUser'}">
                        <ul class="nav navbar-nav navbar-right">                        
                            <li><a href="${pageContext.request.contextPath}/connexion">connexion</a></li>
                            <li><a href="${pageContext.request.contextPath}/inscription">inscription</a></li>
                        </ul>
                    </c:if>
                    <c:if test="${sessionScope.userSession != 'defaultUser'}">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="${pageContext.request.contextPath}/preferences">profil</a></li>
                            <li><a href="${pageContext.request.contextPath}/deconnexion">déconnexion</a></li>
                        </ul>
                    </c:if>
                </div>
            </div>
        </nav>
        <div class="container">

