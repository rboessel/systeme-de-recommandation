<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp">
    <jsp:param name="title" value="Préférences"/>
</jsp:include>
<h2>${user.name}</h2>

<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">Profil</div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>nom</th>
                    <th>e-mail</th>
                </tr>
            </thead>
            <tr>
                <td>${user.name}</td>
                <td>${user.email}</td>
                <td><a class="btn btn-info" href="${pageContext.request.contextPath}/creation">Créer l'index</a></th>
            </tr>
        </table>
    </div>
    <div class="panel-heading">Vos recherche préférées</div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Catégories préférées</th>
                    <th>Catégories moins aimés</th>
                </tr>
            </thead>
            <c:if test="${not empty user.classement}" >    
                <c:forEach items="${user.classement}" var="c" >
                    <tr>
                        <td>${c.strongCategory.name}</td>
                        <td>
                            <ul>
                                <c:forEach items="${c.weakCategories}" var="w" >
                                    <li>${w.name}</li>
                                    </c:forEach>
                            </ul>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>

        </table>
    </div>
    <div class="panel-heading">Recherche préférées de tous les utilisateurs</div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Catégories préférées</th>
                    <th>Catégories moins aimés</th>
                </tr>
            </thead>
            <c:if test="${not empty userDefault.classement}" >    
                <c:forEach items="${userDefault.classement}" var="c" >
                    <tr>
                        <td>${c.strongCategory.name}</th>
                        <td>
                            <ul>
                                <c:forEach items="${c.weakCategories}" var="w" >
                                    <li>${w.name}</li>
                                    </c:forEach>
                            </ul>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>

        </table>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
