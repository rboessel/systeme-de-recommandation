<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="header.jsp">
    <jsp:param name="title" value="Accueil"/>
</jsp:include>
<h2>Moteur de recherche</h2>
<div class="highlight">
    <jsp:include page="form/search.jsp"></jsp:include>
    </div>
<c:choose>       
    <c:when test="${not empty results}">
        <div class="text-center"><div class="label label-default">${length} résultats</div></div></br></br>
        <div>
            <c:forEach items="${results}" var="p" varStatus="status">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <a target="_blank" class="panel-heading"
                       href="${pageContext.request.contextPath}/tri/${p.urlName}?before=[<c:forEach begin="0" end="${status.index}" items="${results}" var="pag" varStatus="i" ><c:if test="${i.index != status.index}">${pag.name}<c:if test="${i.index != status.index-1}">,</c:if></c:if></c:forEach>]
                       &url=${p.url}">

                        ${p.name}
                    </a>
                    <div class="panel-body">
                        <p><strong>Description:</strong></p>
                        <p>${p.description}</p>
                    </div>
                    <!-- List group -->
                    <ul class="list-group">
                        <li class="list-group-item">
                            <strong>Images: </strong>
                            <c:if test="${p.haveImage}">oui
                            </c:if>
                            <c:if test="${not p.haveImage}">non
                            </c:if>
                            | <strong> Longueur:</strong> 
                            ${p.length} caractères
                            | <strong> Date de création:</strong> 
                            ${p.creationDate}
                        </li>
                        <li class="list-group-item">
                            <p><strong>Catégories:</strong></p>
                            <c:forEach items="${p.categories}" var="c" varStatus="status">
                                <p>${c.name}</p>
                            </c:forEach>
                        </li>
                    </ul>
                </div>
            </c:forEach>
        </div>
    </c:when>
    <c:when test="${pageContext.request.method == 'POST'}"><div class="text-center"><div class="label label-default">Aucun résultat</div></div></c:when>
</c:choose>
<jsp:include page="footer.jsp"></jsp:include>
