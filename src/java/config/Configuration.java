package config;

/**
 * Regroupe les différentes variables nécessaires à la configuration.
 * 
 * @author 21205282
 */
public interface Configuration {
    public static String FREEBASE_KEY = "AIzaSyAzmDTE3I3jFlfRGwumH1SFKDhwcqMDLPw";
    public static String USER_SESSION = "userSession";
    public static String CONTEXT = "/home/romain/Bureau/recommandation/src";
    public static String WIKI_BD = CONTEXT+"/BDD/bdd/wiki.test.xml";
    public static String CLEAN_BD = CONTEXT+"/BDD/index/index.xml";
    public static String LUCENE_INDEX = CONTEXT+"/BDD/index/index";

}
