package recommandation.handler;

import recommandation.items.Category;
import recommandation.items.CategoryClassement;
import recommandation.items.Page;
import java.util.ArrayList;
import java.util.List;

/**
 * Fonctions utiles à {@link recommandation.controllers.SearchController}
 *
 * @author romain
 */
public class SearchHandler {

    /**
     * Tri les pages de résultats dans l'ordre où elles doivent apparaître
     *
     * @param pages liste des pages
     * @param cc classement
     * @return pages dans le bon ordre
     */
    public static Page[] sort(Page[] pages, List<CategoryClassement> cc) {
        // lit le tableau par la fin.
        for (int i = pages.length - 1; i >= 0; i--) {
            //récupére toutes les catégories plus faible d'une page. 
            List<Category> weakCategories = getWeakCategories(pages, i, cc);
            //lit le tableau jusqu'à la position actuelle
            pages = checkPreviousElement(pages, i, weakCategories);
        }
        return pages;
    }

    /**
     * Vérifie que les éléments précédents i dans le tableau page son plus
     * importants ou neutre. Sinon déplace i au dessus des éléments moins
     * importants.
     *
     * @param pages pages de résultat
     * @param i index de la page à trier
     * @param weakCategories liste des catégorie moins importantes de la page
     * pages[i]
     * @return liste des pages de résultats
     */
    private static Page[] checkPreviousElement(Page[] pages, int i, List<Category> weakCategories) {
        checkLoop:
        for (int e = 0; e < i; e++) {
            //décaler tout le tableau.
            for (Category wc : weakCategories) {
                for (Category c : pages[e].getCategories()) {
                    if (c.getName().equals(wc.getName())) {
                        pages = moveElement(pages, i, e);
                        i++;
                        break checkLoop;
                    }
                }
            }
        }
        return pages;
    }

    /**
     * Déplace un élément du tableau de page
     *
     * @param pages liste des pages de résultat
     * @param actualI index courant
     * @param nextI index à déplacer
     * @return tableau de page
     */
    private static Page[] moveElement(Page[] pages, int actualI, int nextI) {
        Page p = pages[actualI];
        if (nextI < actualI) {
            for (int i = actualI; i > nextI; i--) {
                pages[i] = pages[i - 1];
            }
        } else {
            for (int i = actualI; i < nextI; i++) {
                pages[i] = pages[i + 1];
            }
        }
        pages[nextI] = p;
        return pages;
    }

    /**
     * Récupère toutes les catégories moins importantes de la page
     *
     * @param pages résultat de la recherche
     * @param i index courant
     * @param cc Liste des classement de catégories
     * @return catégories moins importantes de la page
     */
    private static List<Category> getWeakCategories(Page[] pages, int i, List<CategoryClassement> cc) {
        List<Category> weakCategories = new ArrayList<>();
        for (CategoryClassement actualCc : cc) {
            //Quand une catégorie de la page séléctionné est classée
            for (Category c : pages[i].getCategories()) {
                if (c.getName().equals(actualCc.getStrongCategory().getName())) {
                    weakCategories.addAll(actualCc.getWeakCategories());
                }
            }
        }
        return weakCategories;
    }
}
