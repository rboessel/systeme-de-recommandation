package recommandation.handler;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import recommandation.items.Category;
import recommandation.items.Page;
import recommandation.items.PageDb;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import recommandation.user.UserRecommand;

/**
 * Bibliothèque de fonctions pour le tri
 *
 * @author romain
 */
public class SortHandler {

    /**
     * Récupère un liste de nom de page au format JSON et la connexion à la BDD
     * pour renvoyer les {@link recommandation.items.Page} correspondantes
     *
     * @param jsonBefore tableau de page au format JSON
     * @param pageDb Gestionnaire des pages en BDD
     * @return liste de pages
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    public static ArrayList<Page> getPagesBefore(String jsonBefore, PageDb pageDb)throws UnsupportedEncodingException{
        JSONArray arrayBefore = new JSONArray(jsonBefore);
        ArrayList<Page> pagesBefore = new ArrayList<>();
        for (int i = 0; i < arrayBefore.length(); i++) {
            String name = arrayBefore.getString(i);
            String decodeName = URLDecoder.decode(name, "ISO-8859-1");
            pagesBefore.add(
                    pageDb.getByName(decodeName));
        }
        return pagesBefore;
    }

    /**
     * Récupère les catégories des pages entrées en arguments excepté celles qui
     * appartiennent à la page actuelle
     *
     * @param pagesBefore Page précédente
     * @param currentPage Page sélectionnée
     * @return liste des catégories des pages précédentes
     */
    public static ArrayList<Category> getBeforeCategories(List<Page> pagesBefore, Page currentPage) {
        ArrayList<Category> beforeCat = new ArrayList<>();
        for (Page p : pagesBefore) {
            beforeCat.removeAll(p.getCategories());
            beforeCat.addAll(p.getCategories());
        }
        beforeCat.removeAll(currentPage.getCategories());
        return beforeCat;
    }

    /**
     * Vérifie que la catégorie testé est moins importante ou neutre pour
     * l'utilisateur et peut donc être ajouter au catégorie moins importante de
     * la catégorie actuelle
     *
     * @param currentCat catégorie actuelle de la page sélectionnée
     * @param testCat Catégorie à tester
     * @param user utilisateur connecté
     * @return booléen
     */
    private static boolean canSave(Category currentCat, Category testCat, UserRecommand user) {
        return !isStronger(currentCat, testCat, user);
    }

    /**
     * Teste récursivement que la catégorie testé est plus importante pour
     * l'utilisateur
     *
     * @param currentCat catégorie actuelle de la page sélectionnée
     * @param testCat Catégorie à tester
     * @param user utilisateur connecté
     * @return booléen
     */
    private static boolean isStronger(Category currentCat, Category testCat, UserRecommand user) {
        List<Category> cats = user.getStrongerCategories(currentCat);
        for (Category cat : cats) {
            if (cat.equals(testCat) || isStronger(cat, testCat, user)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sélectionne et ajoute le contenu aux
     * {@link recommandation.items.CategoryClassement} de l'utilisateur
     *
     * @param beforeCat Catégories moins importantes sur cette recherche
     * @param currentPageCat Catégories de la page sélectionnée par
     * l'utilisateur
     * @param user utilisateur connecté
     */
    public static void addUserClassement(List<Category> beforeCat, List<Category> currentPageCat, UserRecommand user) {
        //pour chaque catégories de la page actuelle
        beforeCat.removeAll(currentPageCat);
        List<Category> toDelete = new ArrayList<>();
        for (Category currentCat : currentPageCat) {
            for (Category bC : beforeCat) {
                if (!canSave(currentCat, bC, user)) {
                    toDelete.add(bC);
                }
            }
        }
        beforeCat.removeAll(toDelete);
        for (Category currentCat : currentPageCat) {
            user.addCategoryClassement(currentCat, beforeCat);
        }
    }
}
