package recommandation.controllers;

import recommandation.items.Category;
import recommandation.items.Page;
import recommandation.items.PageDb;
import static config.Configuration.*;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import recommandation.handler.SortHandler;
import recommandation.user.UserRecommand;
import recommandation.user.UserRecommandDb;

/**
 * Gère le classement des Catégories
 *
 * @author romain
 */
@Controller
public class SortController {

    /**
     * Met à jour les
     * {@link recommandation.user.UserRecommand#getClassement() | classements}
     *
     * @param name nom de la {@link recommandation.items.Category} choisie
     * @param req requête
     * @return la page Wikipédia de correspondant à l'article.
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    @RequestMapping(value = "/tri/{name}", produces = "text/plain;charset=UTF-8")
    public String sort(@PathVariable(value = "name") String name, HttpServletRequest req) throws UnsupportedEncodingException {
        String url;
        url = req.getParameter("url");
        url = URLEncoder.encode(url, "ISO-8859-1");
        SessionFactory sessionFactory = (SessionFactory) req.getServletContext().getAttribute("sessionFactory");
        PageDb pageDb = new PageDb(sessionFactory);
        ArrayList<Page> pagesBefore = SortHandler.getPagesBefore(req.getParameter("before"), pageDb);
        Page currentPage = pageDb.getByName(name);
        UserRecommandDb userDb = new UserRecommandDb(sessionFactory);
        String userName = (String) req.getSession().getAttribute(USER_SESSION);
        addClassementUser(userName, pagesBefore, currentPage, userDb);
        if (!name.equals("defaultUser")) {
            addClassementUser("defaultUser", pagesBefore, currentPage, userDb);
        }
        return "redirect:http://fr.wikipedia.org/wiki/" + url;
    }

    /**
     * Se charge de sélectionner les informations à envoyer à la base de données
     * et de mettre à jour l'utilisateur
     *
     * @param name nom de l'utilisateur
     * @param pagesBefore pages moins importantes pour cette recherche
     * @param currentPage page actuelle
     * @param userDb gestion des utilisateurs en base
     */
    private static void addClassementUser(String name, ArrayList<Page> pagesBefore, Page currentPage, UserRecommandDb userDb) {
        UserRecommand userdefault = userDb.getByName(name);
        ArrayList<Category> beforeCatdefault = SortHandler.getBeforeCategories(pagesBefore, currentPage);
        List<Category> currentPageCatdefault = currentPage.getCategories();
        SortHandler.addUserClassement(beforeCatdefault, currentPageCatdefault, userdefault);
        userDb.update(userdefault);
    }
}
