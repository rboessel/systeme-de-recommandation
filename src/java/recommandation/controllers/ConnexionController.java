package recommandation.controllers;

import static config.Configuration.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import recommandation.form.ConnexionForm;
import recommandation.user.UserRecommand;
import recommandation.user.UserRecommandDb;

/**
 * Gère les pages de Connexion d'un l'utilisateur
 *
 * @author romain
 */
@Controller
public class ConnexionController {

    /**
     * GET : Affiche le formulaire de connexion
     *
     * @param model modèle
     * @return formulaire de connexion
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/connexion", method = RequestMethod.GET)
    public String connexionGet(Model model)
            throws ServletException, IOException {
        model.addAttribute("connexion", new ConnexionForm());
        return "form/connexion";
    }

    /**
     * POST : Gère la récupération des informations de connexion et des erreurs.
     * Connecte utilisateur si tout c'est bien passé.
     *
     * @param connexionForm Bean qui stocke le formulaire.
     * @param result gestion des erreurs
     * @param model modèle
     * @param req requête
     * @return Si il y a des erreurs retourne le formulaire, sinon la page
     * d'accueil
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/connexion", method = RequestMethod.POST)
    public String connexionPost(@ModelAttribute("connexion") @Valid ConnexionForm connexionForm, BindingResult result, Model model, HttpServletRequest req)
            throws ServletException, IOException {
        String name = connexionForm.getName();
        String password = connexionForm.getPassword();
        model.addAttribute("name", name);
        model.addAttribute("password", password);
        if (!result.hasErrors()) {
            UserRecommandDb userDb = new UserRecommandDb((SessionFactory) req.getServletContext().getAttribute("sessionFactory"));
            if (userDb.userExist(name)) {
                UserRecommand user = (UserRecommand) userDb.getByName(name);
                if (user.isPassword(password)) {
                    model.asMap().clear();
                    req.getSession().setAttribute(USER_SESSION, user.getName());
                    return "redirect:/";
                } else {
                    result.rejectValue("", "error.object", "Ce n'est pas le bon mot de passe.");
                }
            } else {
                result.rejectValue("", "error.object", "L'utilisateur n'existe pas.");
            }
        }
        return "form/connexion";
    }

    /**
     * Déconnecte l'utilisateur et le remplace par l'utilisateur par défaut
     *
     * @param req requête
     * @param resp réponse
     * @return redirige vers la page d'accueil
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/deconnexion")
    public String deconnexion(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getSession().removeAttribute(USER_SESSION);
        req.getSession().setAttribute(USER_SESSION, "defaultUser");
        return "redirect:/";
    }

}
