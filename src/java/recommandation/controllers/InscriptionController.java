package recommandation.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import recommandation.form.InscriptionForm;
import recommandation.user.UserRecommand;
import recommandation.user.UserRecommandDb;

/**
 * Gère l'inscription des utilisateurs
 *
 * @author romain
 */
@Controller
public class InscriptionController {

    /**
     * GET: Gère le formulaire d'inscription
     *
     * @param model modèle
     * @return formulaire d'inscription
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/inscription", method = RequestMethod.GET)
    public String inscriptionGet(Model model)
            throws ServletException, IOException {
        model.addAttribute("inscription", new InscriptionForm());
        return "form/inscription";
    }

    /**
     * POST : Gère la récupération des informations d'inscription et des
     * erreurs. Inscrit l'utilisateur si tout c'est bien passé.
     *
     * @param inscriptionForm Bean qui stocke le formulaire
     * @param result gestion des erreurs
     * @param model modèle
     * @param req requête
     * @return Si il y a des erreurs retourne le formulaire, sinon la page de
     * validation
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/inscription", method = RequestMethod.POST)
    public String inscriptionPost(@ModelAttribute("inscription") @Valid InscriptionForm inscriptionForm, BindingResult result, Model model, HttpServletRequest req)
            throws ServletException, IOException {
        String name = inscriptionForm.getName();
        String password = inscriptionForm.getPassword();
        String email = inscriptionForm.getEmail();
        model.addAttribute("name", name);
        model.addAttribute("password", password);
        model.addAttribute("email", email);
        if (name.equals(password)) {
            result.rejectValue("", "error.object", "L'utilisateur existe déjà.");
        }
        if (!result.hasErrors()) {
            UserRecommand user = new UserRecommand(name, password);
            user.setEmail(email);
            UserRecommandDb userDb = new UserRecommandDb((SessionFactory) req.getServletContext().getAttribute("sessionFactory"));
            if (userDb.userExist(name)) {
                result.rejectValue("", "error.object", "L'utilisateur existe déjà.");
            } else {
                userDb.add(user);
                return "form/validInscription";
            }
        }
        return "form/inscription";
    }

}
