package recommandation.controllers;

import recommandation.items.Page;
import recommandation.items.PageDb;
import static config.Configuration.*;
import static config.Configuration.WIKI_BD;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.lucene.queryparser.classic.ParseException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import recommandation.form.SearchForm;
import recommandation.handler.SearchHandler;
import recommandation.user.UserRecommand;
import recommandation.user.UserRecommandDb;
import searchengine.SearchEngine;

/**
 * Gère la recherche
 *
 * @author romain
 */
@Controller
public class SearchController {

    /**
     * GET: Affiche le formulaire de recherche
     *
     * @param model modèle
     * @param req requête
     * @return le résultat de la recherche
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    @RequestMapping(value = {"/", "/recherche"}, method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest req) throws UnsupportedEncodingException {
        model.addAttribute("search", new SearchForm());
        String error = (String) model.asMap().get("error");
        if (error != null) {
            req.setAttribute("error", error);
        }
        return "index";
    }

    /**
     * POST: Affiche le formulaire de recherche et le résultat de la recherche
     *
     * @param searchForm Bean qui stocke le formulaire
     * @param result  gestion des erreurs
     * @param model modèle
     * @param req requête
     * @param redirAttr Attributs conservés après la redirection
     * @return le formulaire de recherche
     * @throws UnsupportedEncodingException UnsupportedEncodingException
     */
    @RequestMapping(value = {"/", "/recherche"}, method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String search(@ModelAttribute("search") SearchForm searchForm, BindingResult result,
            Model model, HttpServletRequest req, RedirectAttributes redirAttr) throws UnsupportedEncodingException {
        SessionFactory sessionFactory = (SessionFactory) req.getServletContext().getAttribute("sessionFactory");
        String query = searchForm.getQuery();
        model.addAttribute("query", query);
        req.setCharacterEncoding("UTF-8");
        SearchEngine searchEngine = new SearchEngine(WIKI_BD, CLEAN_BD, LUCENE_INDEX);
        try {
            List<String> searchResult = searchEngine.search(query);
            //récupére les pages du résultat
            PageDb pageDb = new PageDb(sessionFactory);
            Page[] pages = new Page[searchResult.size()];
            int i = 0;
            for (String pageName : searchResult) {
                pages[i] = pageDb.getByName(pageName);
                i++;
            }
            //récupére l'utilisateur courant
            UserRecommandDb userDb = new UserRecommandDb(sessionFactory);
            UserRecommand user = (UserRecommand) userDb.getByName((String) req.getSession().getAttribute(USER_SESSION));
            //tri les pages
            pages = SearchHandler.sort(pages, user.getClassement());
            //injecte les pages dans la requête
            req.setAttribute("results", pages);
            req.setAttribute("length", pages.length);
        } catch (ParseException | IOException | java.text.ParseException ex) {
            result.rejectValue("", "error.object", ex.getMessage());
        }
        return "index";
    }

}
