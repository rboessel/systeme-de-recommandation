package recommandation.controllers;

import recommandation.articletopage.Reader;
import static config.Configuration.*;
import static config.Configuration.WIKI_BD;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;
import recommandation.user.UserRecommandDb;
import searchengine.SearchEngine;

/**
 * Gère les préférences des utilisateurs
 *
 * @author romain
 */
@Controller
public class ProfileController {

    /**
     * page des préférences
     *
     * @param req requête
     * @param resp réponse
     * @return la page des préférences
     * @throws ServletException ServletException
     * @throws IOException IOException
     */
    @RequestMapping(value = "/preferences")
    public String preferences(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        UserRecommandDb userDb = new UserRecommandDb((SessionFactory) req.getServletContext().getAttribute("sessionFactory"));
        req.setAttribute("user", userDb.getByName((String) req.getSession().getAttribute(USER_SESSION)));
        req.setAttribute("userDefault", userDb.getByName("defaultUser"));
        return "preferences";
    }

    /**
     * Ajoute les {@link recommandation.items.Page} à la base de donnée grâce au
     * méthode du package {@link searchengine}
     *
     * @param redirectAtt Attributs conservés après la redirection
     * @param req requête
     * @return redirection vers la page d'accueil
     */
    @RequestMapping(value = "/creation")
    public String creation(final RedirectAttributes redirectAtt, HttpServletRequest req) {
        try {
            SearchEngine searchEngine = new SearchEngine(WIKI_BD, CLEAN_BD, LUCENE_INDEX);
            searchEngine.wikiparse();
            searchEngine.indexer();
            Reader reader = new Reader(CLEAN_BD, FREEBASE_KEY, (SessionFactory) req.getServletContext().getAttribute("sessionFactory"));
            reader.create();
        } catch (SAXException | ParserConfigurationException | IOException ex) {
            redirectAtt.addFlashAttribute("error", ex.getMessage());
        }
        return "redirect:/";
    }

}
