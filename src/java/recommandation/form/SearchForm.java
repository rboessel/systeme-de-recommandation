package recommandation.form;

/**
 * Stocke les informations du formulaire de recherche
 *
 * @author romain
 */
public class SearchForm {

    public String query;

    /**
     * Constructeur vide pour Spring
     */
    public SearchForm() {

    }

    /**
     * Getter de la recherche
     *
     * @return recherche
     */
    public String getQuery() {
        return query;
    }

    /**
     * Setter de la recherche
     *
     * @param query recherche
     */
    public void setQuery(String query) {
        this.query = query;
    }

}
