package recommandation.form;

import javax.validation.constraints.*;

/**
 * Stocke les informations du formulaire de connexion
 *
 * @author 21205282
 */
public class ConnexionForm {

    @NotNull(message = "Le mot de passe doit être indiqué.")
    @Size(min = 1, message = "Le mot de passe ne peut pas être vide.")
    public String password;
    @NotNull(message = "Le nom doit être indiqués.")
    @Size(min = 1, message = "Le nom ne peut pas être vide.")
    public String name;

    /**
     * Constructeur vide pour Spring
     */
    public ConnexionForm() {
    }

    /**
     * Getter du mot de passe
     *
     * @return mot de passe
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter du mot de passe
     *
     * @param password mot de passe
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter du nom d'utilisateur
     *
     * @return nom d'utilisateur
     */
    public String getName() {
        return name;
    }

    /**
     * Setter du nom d'utilisateur
     *
     * @param name nom d'utilisateur
     */
    public void setName(String name) {
        this.name = name;
    }

}
