package recommandation.form;

import javax.validation.constraints.*;

/**
 * Stocke les informations du formulaire d'inscription
 *
 * @author 21205282
 */
public class InscriptionForm extends ConnexionForm {

    @NotNull(message = "L'email doit être indiqué")
    @Size(min = 1, message = "L'email ne peut pas être vide.")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$", message = "Il faut entrer un e-mail valide.")
    public String email;

    /**
     * Constructeur vide pour Spring
     */
    public InscriptionForm() {
    }

    /**
     * Getter de l'émail
     *
     * @return émail
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter de l'émail
     *
     * @param email émail
     */
    public void setEmail(String email) {
        this.email = email;
    }

}
