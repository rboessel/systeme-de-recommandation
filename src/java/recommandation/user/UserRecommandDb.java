package recommandation.user;

import recommandation.items.CategoryClassement;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import recommandation.items.Category;

/**
 * Permet de récupérer les {@link recommandation.user.UserRecommand} en BDD.
 *
 * @author romain
 */
public class UserRecommandDb {

    private final SessionFactory sessionFactory;

    /**
     * Constructeur
     *
     * @param sessionFactory connexion à la BDD
     */
    public UserRecommandDb(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Ajoute un utilisateur à la BDD
     *
     * @param user utilisateur à ajouter
     */
    public void add(UserRecommand user) {
        UserRecommand[] users = new UserRecommand[1];
        users[0] = user;
        add(users);
    }

    /**
     * Ajoute des utilisateurs à la BDD
     *
     * @param users utilisateurs à ajouter
     */
    public void add(UserRecommand[] users) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (UserRecommand user : users) {
                session.saveOrUpdate(user);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
        }
    }
    
      /**
     * Met à jour un utilisateur dans la BDD
     *
     * @param user utilisateur à mettre à jour
     */
    public void update(UserRecommand user) {
        System.out.println(user.getName());
        UserRecommand[] users = new UserRecommand[1];
        users[0] = user;
        update(users);
    }


    /**
     * Met à jour des utilisateurs dans la BDD
     * <p>Utilise la fonction merge()</p>
     *
     * @param users utilisateurs à mettre à jour
     */
    public void update(UserRecommand[] users) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (UserRecommand user : users) {
                session.merge(user);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Récupère un utilisateur par son nom
     *
     * @param name nom de l'utilisateur à récupérer
     * @return l'utilisateur ou null
     */
    public UserRecommand getByName(String name) {
        Session session = sessionFactory.openSession();
        String query = "from UserRecommand where name like :name";
        UserRecommand result = (UserRecommand) session.createQuery(query).setParameter("name", name).uniqueResult();
        if (result != null) {
            Hibernate.initialize(result.getClassement());
            if (result.getClassement() != null) {
                for (CategoryClassement cc : result.getClassement()) {
                    Hibernate.initialize(cc.getWeakCategories());
                    for (Category c : cc.getWeakCategories()) {
                        Hibernate.initialize(c.getName());
                    }
                }
            }

        }
        session.close();
        return result;
    }

    /**
     * teste si un utilisateur existe
     *
     * @param name nom de l'utilisateur
     * @return true si il existe false sinon
     */
    public boolean userExist(String name) {
        return getByName(name) != null;
    }

}
