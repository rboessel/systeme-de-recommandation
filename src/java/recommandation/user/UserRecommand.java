package recommandation.user;

import recommandation.items.Category;
import recommandation.items.CategoryClassement;
import java.util.ArrayList;
import java.util.List;
import user.User;

/**
 * Stocke les caractéristiques et les préférences de l'utilisateur
 *
 * @author 21205282
 */
public class UserRecommand extends User {

    protected List<CategoryClassement> classement;

    /**
     * Constructeur vide pour Hibernate
     */
    public UserRecommand() {

    }

    /**
     * Constructeur
     *
     * @param name nom de l'utilisateur
     * @param password mot de passe de l'utilisateur
     */
    public UserRecommand(String name, String password) {
        super(name, password);
        this.classement = new ArrayList<>();
    }

    /**
     * Getter de la liste d'ordre d'importance des catégories
     *
     * @return Liste d'ordre d'importance des catégories
     */
    public List<CategoryClassement> getClassement() {
        return classement;
    }

    /**
     * Setter de la liste d'ordre d'importance des catégories
     *
     * @param classement liste d'ordre d'importance des catégories
     */
    public void setClassement(List<CategoryClassement> classement) {
        this.classement = classement;
    }

    /**
     * Ajoute un classement à la liste
     *
     * @param strong catégorie préférée
     * @param weak liste de catégories moins importantes
     */
    public void addCategoryClassement(Category strong, List<Category> weak) {
        if (!weak.isEmpty()) {
            for (CategoryClassement cc : classement) {
                if (cc.getStrongCategory().equals(strong)) {
                    weak.removeAll(cc.getWeakCategories());
                    cc.getWeakCategories().addAll(weak);
                    return;
                }
            }
            classement.add(new CategoryClassement(strong, weak));
        }
    }

    /**
     * Getter des catégories préférées de celle passé en argument
     *
     * @param c catégorie à analyser
     * @return catégories préférées de celle passé en argument
     */
    public List<Category> getStrongerCategories(Category c) {
        List<Category> stronger = new ArrayList<>();
        for (CategoryClassement cc : classement) {
            if (cc.getWeakCategories().contains(c)) {
                stronger.add(cc.getStrongCategory());
            }
        }
        return stronger;
    }
}
