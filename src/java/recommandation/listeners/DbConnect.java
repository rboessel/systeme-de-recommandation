package recommandation.listeners;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import org.hibernate.service.ServiceRegistryBuilder;
import recommandation.user.UserRecommand;
import recommandation.user.UserRecommandDb;

/**
 * Active la connexion à la BDD au déclenchement de la servlet.
 *
 * @author romain
 */
public class DbConnect implements ServletContextListener {

    /**
     * Active la connexion à la BDD
     *
     * @param event contexte de l'initialisation
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        event.getServletContext().setAttribute("sessionFactory", configuration.buildSessionFactory(serviceRegistry));
        UserRecommandDb userDb = new UserRecommandDb((SessionFactory) event.getServletContext().getAttribute("sessionFactory"));
        UserRecommand user = userDb.getByName("defaultUser");
        if (user == null) {
            userDb.add(new UserRecommand("defaultUser", "default"));
        }
    }

    /**
     * Ferme la connexion à la fermeture de la Servlet
     *
     * @param event contexte de la destruction
     */
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (event.getServletContext().getAttribute("sessionFactory") != null) {
            ((SessionFactory) event.getServletContext().getAttribute("sessionFactory")).close();
        }
    }

}
