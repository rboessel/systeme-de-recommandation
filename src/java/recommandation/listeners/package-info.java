/**
 * Liste des listeners qui se lancent lorsque certains événements sont lancés par
 * l'application
 */
package recommandation.listeners;
