package recommandation.filters;

import static config.Configuration.USER_SESSION;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Filtre s'assurant de rediriger les utilisateurs non connectés vers la page de {@link recommandation.controllers.ConnexionController#connexionGet(org.springframework.ui.Model)
 *  connexion}
 *
 * @author romain
 */
public class SecurityFilter implements Filter {

    FilterConfig filterConfig = null;

    /**
     * S'active à l'activation du filtre
     *
     * @param fc configuration du filtre
     * @throws ServletException ServletException
     */
    @Override
    public void init(FilterConfig fc) throws ServletException {
        this.filterConfig = fc;
    }

    /**
     * Se lance au chargement de chaque page :
     * <p>
     * Redirige les utilisateurs non connectés vers la page de {@link recommandation.controllers.ConnexionController#connexionGet(org.springframework.ui.Model)
     *  connexion} </p>
     *
     * @param sr requête
     * @param sr1 réponse
     * @param fc FilterChain
     * @throws IOException IOException
     * @throws ServletException ServletException
     */
    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sr;
        HttpSession session = req.getSession();
        if (session.getAttribute(USER_SESSION) == null || session.getAttribute(USER_SESSION).equals("defaultUser")) {
            HttpServletResponse httpResponse = (HttpServletResponse) sr1;
            httpResponse.sendRedirect("connexion");
            return;
        }
        fc.doFilter(sr, sr1);
    }

    /**
     *
     */
    @Override
    public void destroy() {
    }

}
