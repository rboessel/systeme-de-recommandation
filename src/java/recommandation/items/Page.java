package recommandation.items;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Permet de stocker les caractéristique des pages Wikipédia à retourner.
 *
 * @author romain
 */
public class Page {

    protected String name;
    protected String description;
    protected List<Category> categories;
    protected int length;
    protected Date creationDate;
    protected Boolean haveImage;

    /**
     * Constructeur vide pour Hibernate
     */
    public Page() {

    }

    /**
     * Constructeur
     *
     * @param name Nom de la page wikipédia
     * @param description Description de l'article
     * @param categories Catégories de l'article
     * @param time Date de création
     * @param length Longueur de l'article
     * @param haveImage Teste si l'article contient des images
     */
    public Page(String name, String description, List<Category> categories, Date time, int length, boolean haveImage) {
        this.categories = categories;
        this.description = description;
        this.name = name;
        this.creationDate = time;
        this.length = length;
        this.haveImage = haveImage;
    }

    /**
     * Getter des catégories
     *
     * @return catégories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * Setter des catégories
     *
     * @param mainCat Catégories
     */
    public void setCategories(List<Category> mainCat) {
        this.categories = mainCat;
    }

    /**
     * Getter du nom
     *
     * @return nom
     */
    public String getName() {
        return name;
    }

    /**
     * Setter du nom
     *
     * @param name nom
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter de la longueur
     *
     * @return longueur
     */
    public int getLength() {
        return length;
    }

    /**
     * Setter de la longueur
     *
     * @param length longueur
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Getter de la date de création
     *
     * @return date de création
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Setter de la date de création
     *
     * @param time date de création
     */
    public void setCreationDate(Date time) {
        this.creationDate = time;
    }

    /**
     * Getter de la description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter de la description
     *
     * @param description description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter du test si l'article contient des images
     *
     * @return true si l'article contient des images, false sinon
     */
    public Boolean getHaveImage() {
        return haveImage;
    }

    /**
     * Setter du test si l'article contient des images
     *
     * @param images booléen de réponse au test
     */
    public void setHaveImage(Boolean images) {
        this.haveImage = images;
    }

    /**
     * Renvoie l'url
     *
     * @return url nom avec des "_" à la place des espaces pour l'url.
     */
    public String getUrl() {
        return name.replaceAll(" ", "_");
    }

    /**
     * Convertie le nom en ISO- 8859-1 pour conserver les accents après passage
     * dans l'URL
     *
     * @return nom au format ISO-8859-1
    */
    public String getUrlName() {
        try {
            String stringISO = new String(name.getBytes(), "ISO-8859-1");
            return URLEncoder.encode(stringISO, "ISO-8859-1");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Page.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    } 
}
