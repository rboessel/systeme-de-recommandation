/**
 * Liste des items nécessaires au système de recommandation avec leur
 * gestionnaire d'accès à la base de donnée
 */
package recommandation.items;
