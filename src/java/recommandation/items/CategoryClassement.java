package recommandation.items;

import java.util.List;

/**
 * Stocke une catégorie et la liste de celles qui sont moins importantes afin
 * d'établir un classement
 *
 * @author romain
 */
public class CategoryClassement {

    private int id;
    private Category strongCategory;
    private List<Category> weakCategories;

    /**
     * Constructeur vide pour hibernate
     */
    public CategoryClassement() {

    }

    /**
     * Constructeur
     *
     * @param strongCategory La catégorie au dessus des autres
     * @param weakCategory Les catégories moins importantes
     */
    public CategoryClassement(Category strongCategory, List<Category> weakCategory) {
        this.strongCategory = strongCategory;
        this.weakCategories = weakCategory;
    }

    /**
     * Getter de la catégorie Importante
     *
     * @return catégorie importante
     */
    public Category getStrongCategory() {
        return strongCategory;
    }

    /**
     * Setter de la catégorie importante
     *
     * @param strongCategory la catégorie importante
     */
    public void setStrongCategory(Category strongCategory) {
        this.strongCategory = strongCategory;
    }

    /**
     * Getter des catégories moins importantes
     *
     * @return les catégories moins importantes
     */
    public List<Category> getWeakCategories() {
        return weakCategories;
    }

    /**
     * Setter des catégories moins importantes
     *
     * @param weakCategory les catégories moins importantes
     */
    public void setWeakCategories(List<Category> weakCategory) {
        this.weakCategories = weakCategory;
    }

    /**
     * Getter de l'Id
     *
     * @return l'Id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter de l'Id
     *
     * @param id Id
     */
    public void setId(int id) {
        this.id = id;
    }

}
