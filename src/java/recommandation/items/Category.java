package recommandation.items;

/**
 * Permet de stocker les Catégories.
 *
 * @author romain
 */
public class Category {

    private String name;

    /**
     * Constructeur vide pour Hibernate
     */
    public Category() {

    }

    /**
     * Constructeur
     *
     * @param name nom de la Catégorie
     */
    public Category(String name) {
        this.name = name;
    }

    /**
     * Getter du nom de la catégorie
     *
     * @return nom de la catégorie
     */
    public String getName() {
        return name;
    }

    /**
     * Setter du nom de la catégorie
     *
     * @param name nom de la catégorie
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Vérifie l'égalité avec un autre objet.
     * <p>
     * Cette méthode utilisé dans les méthodes remove ou contains des List</p>
     * <p>
     * La surchargé permet de définir l'égalité sur les valeurs stockés en base
     * et non sur les instances des catégories</p>
     *
     * @param other objet testé
     * @return booléen
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Category)) {
            return false;
        }
        final Category cat = (Category) other;
        return cat.getName().equals(getName());
    }

    /**
     * "Deux objets qui sont égaux en utilisant la méthode equals() doivent
     * obligatoirement avoir tous les deux la même valeur de retour lors de
     * l'invocation de leur méthode hashCode()"
     * <p>
     * Selon la méthode equals, deux objets de noms identiques en base doivent
     * donc avoir un HashCode() identique.
     * </p>
     *
     * @return le HashCode du nom
     */
    @Override
    public int hashCode() {
        int result;
        result = getName().hashCode();
        return result;
    }
}
