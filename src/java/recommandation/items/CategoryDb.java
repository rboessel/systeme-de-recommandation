package recommandation.items;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Permet de récupérer les {@link Category} en BDD.
 *
 * @author romain
 */
public class CategoryDb {

    private final SessionFactory sessionFactory;

    /**
     * Constructeur
     *
     * @param sessionFactory connexion à la BDD
     */
    public CategoryDb(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Récupère la catégorie à partir de son nom
     *
     * @param name nom de la catégorie
     * @return La catégorie recherchée
     */
    public Category getByName(String name) {
        Session session = sessionFactory.openSession();
        String query = "from Category where name like :name";
        Category result = (Category) session.createQuery(query)
                .setParameter("name", name).uniqueResult();
        session.close();
        return result;
    }

    /**
     * Ajoute ou met à jour la catégorie en BDD
     *
     * @param c Catégorie à ajouter en BDD
     */
    public void add(Category c) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(c);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
        }
    }

}
