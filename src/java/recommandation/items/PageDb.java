package recommandation.items;

import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * Permet de récupérer les {@link Page} en BDD.
 *
 * @author romain
 */
public class PageDb {

    private final SessionFactory sessionFactory;

    /**
     * Constructeur
     *
     * @param sessionFactory Connexion à la BDD
     */
    public PageDb(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Récupère toute les pages en BDD
     *
     * @return Toutes les pages en BDD
     */
    public List<Page> getAll() {
        Session session = sessionFactory.openSession();
        String query = "from Page";
        List<Page> results = session.createQuery(query).list();
        for (Page result : results) {
            if (result != null) {
                Hibernate.initialize(result.getCategories());
            }
        }
        session.close();
        return results;
    }

    /**
     * Récupère une page en BDD par son nom
     *
     * @param name nom de la {@link Page Page} recherchée
     * @return la page correspondante ou null.
     */
    public Page getByName(String name) {
        Session session = sessionFactory.openSession();
        String query = "from Page where name like :name";
        Page result = (Page) session.createQuery(query)
                .setString("name", name).uniqueResult();
        if (result != null) {
            Hibernate.initialize(result.getCategories());
        }
        session.close();
        return result;

    }

    /**
     * Ajoute ou met à jour un page en BDD
     *
     * @param page la page à ajouter/modifier
     */
    public void add(Page page) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.saveOrUpdate(page);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            session.close();
        }
    }

}
