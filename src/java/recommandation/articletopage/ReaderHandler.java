package recommandation.articletopage;

import recommandation.items.Page;
import recommandation.items.PageDb;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.hibernate.SessionFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler de {@link Reader Reader}
 *
 * @author romain
 */
public class ReaderHandler extends DefaultHandler {

    private StringBuffer buffer;
    private Object[] output;
    private final String key;
    private final SessionFactory sessionFactory;

    /**
     * Constructeur
     *
     * @param key clé de l'API
     * @param sessionFactory connexion à la BDD
     */
    public ReaderHandler(String key, SessionFactory sessionFactory) {
        super();
        this.key = key;
        this.output = new Object[3];
        this.sessionFactory = sessionFactory;
    }

    /**
     * Se lance au début du document
     *
     * @throws SAXException SAXException
     */
    @Override
    public void startDocument() throws SAXException {

    }

    /**
     * Se lance au début de chaque balise : active l'enregistrement des
     * caractères pour le titre, la longueur et la date de création et instancie
     * un tableau output en début de page pour récupérer les informations en
     * {@link #endElement(java.lang.String, java.lang.String, java.lang.String)  fin de balise}
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @param attributes attributes
     * @throws SAXException SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("page")) {
            output = new Object[3];
        }
        if (qName.equals("titre") || qName.equals("length") || qName.equals("time")) {
            buffer = new StringBuffer();
        }
    }

    /**
     * Se lance à la fin de chaque balise : récupère les données pour le titre,
     * la longueur et la date de création et les utilisent afin de créer une
     * {@link recommandation.items.Page page} à la fin de chaque balise page puis
     * l'ajoute en BDD
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @throws SAXException SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("page")) {
            System.out.println("");
            Page p = ArticleToPageHandler.createPage((String) output[0], (Date) output[1], (int) output[2], key, sessionFactory);
            PageDb pageDb = new PageDb(sessionFactory);
            pageDb.add(p);
        }
        if (qName.equals("titre")) {
            output[0] = buffer.toString();
            buffer = null;
        }
        if (qName.equals("length")) {
            output[2] = Integer.parseInt(buffer.toString());
            buffer = null;
        }
        if (qName.equals("time")) {
            try {
                String time = buffer.toString();
                time = time.replace("T", " ");
                time = time.replace("Z", " ");
                output[1] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            buffer = null;
        }
    }

    /**
     * Détecte les caractères
     *
     * @param ch ch
     * @param start start
     * @param length length
     * @throws SAXException SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String lecture = new String(ch, start, length);
        if (buffer != null) {
            buffer.append(lecture);
        }
    }

}
