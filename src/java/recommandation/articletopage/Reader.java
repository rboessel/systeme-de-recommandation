package recommandation.articletopage;

import searchengine.indexer.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.hibernate.SessionFactory;
import org.xml.sax.SAXException;

/**
 * Interroge l'index généré par
 * {@link searchengine.parseur.WikiParseur#parseur(java.lang.String, java.lang.String)  le parseur de la base Wikipédia}
 * et <a href="http://freebase.com/" >l'API freebase</a> afin d'entrer les pages
 * et les catégories nécessaires au système de recommandation.
 *
 * @author romain
 */
public class Reader {

    String docPath;
    String key;
    SessionFactory sessionFactory;

    /**
     * Constructeur
     *
     * @param docPath Chemin vers la BDD wikipédia nettoyée
     * @param key clé de l'API
     * @param sessionFactory connexion à la BDD
     */
    public Reader(String docPath, String key, SessionFactory sessionFactory) {
        this.docPath = docPath;
        this.key = key;
        this.sessionFactory = sessionFactory;
    }

    /**
     * Lance le parseur
     */
    public void create() {
        try {
            SAXParserFactory fabrique = SAXParserFactory.newInstance();
            SAXParser parseur = fabrique.newSAXParser();
            File fichier;
            fichier = new File(docPath);
            ReaderHandler gestionnaire = new ReaderHandler(key, sessionFactory);
            parseur.parse(fichier, gestionnaire);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(Indexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
