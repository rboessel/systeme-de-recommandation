package recommandation.articletopage;

import recommandation.items.Category;
import recommandation.items.CategoryDb;
import recommandation.items.Page;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Permet de créer des pages à partir des informations de l'index.
 *
 * @author romain
 */
public class ArticleToPageHandler {

    /**
     * Utilise les articles de l'index fournis par
     * {@link ReaderHandler#endElement(java.lang.String, java.lang.String, java.lang.String)  ReaderHandler} et récupère les catégories
     * sur freebase pour créer une page.
     *
     * @param title nom de la page
     * @param time date de création de l'article
     * @param length longueur de l'article
     * @param key clé de l'API freebase
     * @param sessionFactory la connexion à la BDD
     * @return La page contenant les informations entrées en paramètre
     */
    public static Page createPage(String title, Date time, int length, String key, SessionFactory sessionFactory) {
        String line;
        StringBuilder jsonString = new StringBuilder();
        ArrayList<Category> response = new ArrayList<>();
        String description = null;
        String notable = "";
        String category = "";
        boolean haveImage = false;
        try {
            URL url = new URL("https://www.googleapis.com/freebase/v1/search?query="
                    + title.replace(" ", "_")
                    + "&output=(all)"
                    + "&exact=true&lang=fr&key="
                    + key);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            InputStream input = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(input));
            while ((line = br.readLine()) != null) {
                jsonString.append(line);
            }
            connection.disconnect();
            System.out.println(jsonString.toString());
            JSONObject list = null;
            JSONArray resp = new JSONObject(jsonString.toString())
                    .getJSONArray("result");

            if (!resp.isNull(0)) {
                JSONObject result = resp.getJSONObject(0);
                if (!result.isNull("notable")) {
                    notable = result.getJSONObject("notable").getString("name");
                }
                list = resp
                        .getJSONObject(0)
                        .getJSONObject("output")
                        .getJSONObject("all");

                if (!list.isNull("description./common/topic/description")) {
                    description = list
                            .getJSONArray("description./common/topic/description")
                            .getString(0);
                    //System.out.println(description);
                }
                Iterator<String> s = list.keys();
                while (s.hasNext()) {
                    String actual = s.next();
                    if (actual.matches("category.*")) {
                        category = actual;
                        break;
                    }
                }
                if (!list.isNull(category)) {
                    category = list.getJSONArray(category).getJSONObject(0).getString("name");
                }
                if (!list.isNull("property./common/topic/image")) {
                    if (list.getJSONArray("property./common/topic/image").length() > 0) {
                        haveImage = true;
                    }
                }
                if (notable.isEmpty() && category.isEmpty()) {
                    response.add(getCategory("nullId", sessionFactory));
                }
                if (!notable.isEmpty()) {
                    response.add(getCategory(notable, sessionFactory));
                }
                if (!category.isEmpty()) {
                    response.add(getCategory(category, sessionFactory));
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Page(title, description, response, time, length, haveImage);
    }

    /**
     * Permet de s'assurer qu'une catégorie ne sera pas entrée deux fois en BDD.
     *
     * @param name nom de la catégorie à récupérer
     * @param sessionFactory la connexion à la BDD
     * @return L'objet catégorie stocké en base ou une nouvelle catégorie si
     * elle n'est pas encore entrée
     */
    private static Category getCategory(String name, SessionFactory sessionFactory) {
        CategoryDb cDb = new CategoryDb(sessionFactory);
        Category cat = cDb.getByName(name);
        if (cat == null) {
            cat = new Category(name);
        }
        cDb.add(cat);
        return cat;
    }
}
