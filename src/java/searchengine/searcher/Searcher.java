package searchengine.searcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/**
 * S'occupe des requêtes sur l'index Lucéne
 *
 * @author boessel
 */
public class Searcher {

    IndexReader reader;
    IndexSearcher searcher;
    Query query;
    FrenchAnalyzer analyser;

    /**
     * Constructeur
     *
     * @param indexLocation Chemin vers l'index Lucéne
     * @throws IOException IOException
     * @throws ParseException ParseException
     */
    public Searcher(String indexLocation) throws IOException, ParseException {
        this.reader = DirectoryReader.open(FSDirectory.open(new File(indexLocation)));
        this.searcher = new IndexSearcher(reader);
        this.analyser = new FrenchAnalyzer(Version.LUCENE_40);
    }

    /**
     * Lance un recherche sur le contenu de l'index Lucéne
     *
     * @param q Chaîne de caractères à rechercher
     * @return Liste des noms de page en résultat
     * @throws ParseException ParseException
     * @throws IOException IOException
     * @throws java.text.ParseException java.text.ParseException
     */
    public List<String> search(String q) throws ParseException, IOException, java.text.ParseException {
        return search(q, new String[]{"titre", "entitee"});
    }

    /**
     * Lance une recherche sur un contenu spécifique de l'index Lucéne
     *
     * @param q Chaîne de caractères à rechercher
     * @param queryFields nom des champs à rechercher ("titre" ou "entitee")
     * @return Liste des noms de page en résultat
     * @throws ParseException ParseException
     * @throws IOException IOException
     * @throws java.text.ParseException java.text.ParseException
     */
    public List<String> search(String q, String[] queryFields) throws ParseException, IOException, java.text.ParseException {
        //https://lucene.apache.org/core/4_1_0/queryparser/org/apache/lucene/queryparser/classic/QueryParser.html
        //////////////////////////////////////////////////////////////////////////////////
        this.query = new MultiFieldQueryParser(queryFields, analyser).parse(q);
        //////////////////////////////////////////////////////////////////////////////////
        TopDocs topDocs = searcher.search(query, 9999);
        List<String> results = new ArrayList<>();
        ScoreDoc[] hits = topDocs.scoreDocs;
        for (ScoreDoc hit : hits) {
            Document d = searcher.doc(hit.doc);
            String titre = d.get("titre");
            results.add(titre);
        }
        return results;
    }

}
