package searchengine.indexer;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 * Crée l'index Lucéne sur les entités et les titres de la BDD
 *
 * @author romain
 */
public class Indexer {

    String docPath;
    String indexPath;

    /**
     * Constructeur
     *
     * @param indexPath Chemin vers la BDD nettoyée
     * @param docPath Répertoire de l'index Lucéne
     */
    public Indexer(String indexPath, String docPath) {
        this.docPath = docPath;
        this.indexPath = indexPath;
    }

    /**
     * Lance la création de l'index lucéne
     *
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws SAXException SAXException
     * @throws IOException IOException
     */
    public void create() throws ParserConfigurationException, SAXException, IOException {
        File fichier = new File(docPath);
        if (!fichier.exists() || !fichier.canRead()) {
            System.out.println("Document directory '" + fichier.getAbsolutePath() + "' does not exist or is not readable, please check the path");
            System.exit(1);
        }
        // création d'une fabrique de parseurs SAX 
        SAXParserFactory fabrique = SAXParserFactory.newInstance();
        // création d'un parseur SAX 
        SAXParser parseur = fabrique.newSAXParser();
        IndexHandler gestionnaire;
        gestionnaire = new IndexHandler(indexPath);
        parseur.parse(fichier, gestionnaire);

    }
}
