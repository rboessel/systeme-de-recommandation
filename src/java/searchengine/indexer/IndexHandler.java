package searchengine.indexer;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.document.Document;
import static org.apache.lucene.document.Field.Store.YES;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler de {@link Indexer Indexer}
 *
 * @author romain
 */
public class IndexHandler extends DefaultHandler {

    private StringBuffer buffer;
    private final String indexPath;
    private IndexWriter writer;
    private Document doc;

    /**
     * Constructeur
     *
     * @param indexPath Chemin vers la BDD wikipédia
     */
    public IndexHandler(String indexPath) {
        super();
        this.indexPath = indexPath;
    }

    /**
     * Se lance au début du document. Configure le writter lucéne
     */
    @Override
    public void startDocument() throws SAXException {
        System.out.println("Indexing to directory '" + indexPath + "'...");
        try {
            Directory dir = FSDirectory.open(new File(indexPath));
            Analyzer analyzer = new FrenchAnalyzer(Version.LUCENE_40);
            IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, analyzer);
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            writer = new IndexWriter(dir, iwc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Se lance à chaque ouverture de balise Crée un document lucéne pour chaque
     * nouvelle page et enregistre pour les titres et entités
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @param attributes attributes
     * @throws SAXException SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("page")) {
            doc = new Document();
        }
        if (qName.equals("titre") || qName.equals("e")) {
            buffer = new StringBuffer();
        }
    }

    /**
     * Se lance à chaque fin de balise : Ajoute un TextField pour les titres et
     * entités au Document Lucéne et enregistre le document dans l'index Lucéne
     * à la fin de la page
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @throws SAXException SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("page")) {
            if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
                try {
                    writer.addDocument(doc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            buffer = null;
        }
        if (qName.equals("titre")) {
            doc.add(new TextField("titre", buffer.toString(), YES));
            buffer = null;
        }
        if (qName.equals("e")) {
            doc.add(new TextField("entitee", buffer.toString(), YES));
            buffer = null;
        }
    }

    /**
     * Ferme le writter Lucéne pour l'enregistrer à la fin du document
     *
     * @throws SAXException SAXException
     */
    @Override
    public void endDocument() throws SAXException {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Détecte les caractères
     *
     * @param ch ch
     * @param start start
     * @param length length
     * @throws SAXException SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String lecture = new String(ch, start, length);
        if (buffer != null) {
            buffer.append(lecture);
        }
    }

}
