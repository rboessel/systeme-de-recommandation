package searchengine.parseur;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Handler de {@link WikiParseur WikiParseur}
 *
 * @author romain
 */
public class WikiHandler extends DefaultHandler {

    private StringBuffer buffer;
    private final String fileName;

    /**
     * Constructeur
     *
     * @param fileName nom du ficher à indexer
     */
    public WikiHandler(String fileName) {
        super();
        this.fileName = fileName;
    }

    /**
     * Se lance à chaque ouverture de balise : ouvre un balise
     * &lsaquo;page&rsaquo; pour chaque balise page rencontrée et commence à enregistrer du
     * texte pour chaque balise title, text ou timestamp.
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @param attributes attributes
     * @throws SAXException SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("page")) {
            write("\n<page>\n", fileName);
        }
        if (qName.equals("title")) {
            buffer = new StringBuffer();
        }
        if (qName.equals("text")) {
            buffer = new StringBuffer();
        }
        if (qName.equals("timestamp")) {
            buffer = new StringBuffer();
        }
    }

    /**
     * S'active en fin de balise, ferme la balise page dans le fichier d'index
     * en fin de balise page en lecture. Créer un balise avec le contenu pour
     * les title et timestamp respectivement titre et time et pour la balise
     * texte extrait les entités dans des balises &lsaquo;e&rsaquo; et la
     * longueur dans une balise length
     *
     * @param uri uri
     * @param localName localName
     * @param qName nom de la balise
     * @throws SAXException SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("page")) {
            write("</page>", fileName);
        }
        if (qName.equals("title")) {
            String titre = clean(buffer.toString());
            write("<titre>" + titre + "</titre>", fileName);
            buffer = null;
        }
        if (qName.equals("text")) {
            String text = buffer.toString();
            int length = text.length();
            ArrayList entitees = triCategories(text);
            entitees = uniqueAndSort(entitees);
            for (Object entitee : entitees) {
                String output = clean((String) entitee);
                write("<e>" + output + "</e>", fileName);
            }
            write("<length>" + length + "</length>", fileName);
            buffer = null;
        }
        if (qName.equals("timestamp")) {
            write("<time>" + buffer.toString() + "</time>", fileName);
            buffer = null;
        }
    }

    /**
     * Enregistre les caractères
     *
     * @param ch ch
     * @param start start
     * @param length length
     * @throws SAXException SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String lecture = new String(ch, start, length);
        if (buffer != null) {
            buffer.append(lecture);
        }
    }

    /**
     * Crée une balise &lsaquo;root&rsaquo; au début du document
     *
     * @throws SAXException SAXException
     */
    @Override
    public void startDocument() throws SAXException {
        System.out.println("Début du parsing");
        write("<root>", fileName, false);
    }

    /**
     * Ferme la balise &lsaquo;root&rsaquo; à la fin du document
     *
     * @throws SAXException SAXException
     */
    @Override
    public void endDocument() throws SAXException {
        write("</root>", fileName);
        System.out.println("Fin du parsing");
        System.out.println("Resultats du parsing");
    }

    /**
     * Écrit dans un fichier
     *
     * @param text texte à ajouter
     * @param fileName nom du fichier à écrire
     * @param append ajoute en fin de fichier ou crée un nouveau
     */
    private void write(String text, String fileName, Boolean append) {
        try {
            // Create a new file output stream
            // connected to "myfile.txt"
            FileOutputStream out = new FileOutputStream(fileName, append);
            // Connect print stream to the output stream
            PrintStream p;
            p = new PrintStream(out);
            p.println(text);
            p.close();
        } catch (Exception e) {
            System.err.println("Error writing to file");
        }
    }

    /**
     * Écrit dans un fichier
     *
     * @param text texte à ajouter
     * @param fileName nom du fichier à écrire
     */
    private void write(String text, String fileName) {
        write(text, fileName, true);
    }

    /**
     * Récupère les entités dans un texte
     *
     * @param text texte à analyser
     * @return liste d'entités
     */
    private ArrayList triCategories(String text) {
        ArrayList entitees = new ArrayList();
        Pattern r = Pattern.compile("(\\[\\[[^\\[\\[]*]])");
        Matcher m = r.matcher(text);
        while (m.find()) {
            String entitee = m.group(0);
            entitee = entitee.substring(2, entitee.length() - 2);
            Pattern search = Pattern.compile("(^[^\\|]*)");
            Matcher matcher = search.matcher(entitee);
            if (matcher.find()) {
                entitee = matcher.group(0);
            }
            entitees.add(entitee);
        }
        return entitees;
    }

    /**
     * Échappe les caractères spéciaux
     *
     * @param clean texte à échapper
     * @return texte propre
     */
    private String clean(String clean) {
        clean = clean.replaceAll("&", "&amp;");
        clean = clean.replaceAll("'", "&apos;");
        clean = clean.replaceAll("\"", "&quot;");
        clean = clean.replaceAll("<", "&lt;");
        clean = clean.replaceAll(">", "&gt;");
        if (clean.matches(".*\\]\\].*")) {
            String[] a;
            a = clean.split("\\]\\]");
            clean = a[0];
        }
        return clean;
    }

    /**
     * Vérifie que les entités sont uniques et triées
     *
     * @param entitees entités
     * @return liste d'entités
     */
    private ArrayList uniqueAndSort(ArrayList entitees) {
        Collections.sort(entitees);
        Set set = new HashSet();
        set.addAll(entitees);
        return new ArrayList(set);
    }
}
