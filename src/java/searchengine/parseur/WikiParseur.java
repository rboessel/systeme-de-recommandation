package searchengine.parseur;

import org.xml.sax.*;
import javax.xml.parsers.*;
import java.io.*;

/**
 * Transfère le titre, la description, la date de création et les entités des
 * articles dans un autre fichier XML plus rapide à traiter et plus facilement
 * transportable.
 *
 * @author romain
 */
public class WikiParseur {

    /**
     * Lance {@link WikiHandler WikiHandler}
     *
     * @param wikiFile Chemin vers la base de page Wikipédia
     * @param indexFile Chemin où stocker la base nettoyée
     * @throws SAXException SAXException
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws IOException IOException
     */
    public static void parseur(String wikiFile, String indexFile) throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory fabrique = SAXParserFactory.newInstance();
        SAXParser parseur = fabrique.newSAXParser();
        File fichier = new File(wikiFile);
        WikiHandler gestionnaire = new WikiHandler(indexFile);
        parseur.parse(fichier, gestionnaire);
    }

}
