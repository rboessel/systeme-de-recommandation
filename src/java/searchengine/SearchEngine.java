package searchengine;

import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.xml.sax.SAXException;
import searchengine.indexer.Indexer;
import searchengine.parseur.WikiParseur;
import searchengine.searcher.Searcher;

/**
 * Gère le package searchengine Gère le nettoyage de la BDD wikipédia,
 * l'indexation des page et la récupération des recherches.
 *
 * @author romain
 */
public class SearchEngine {

    private final String wikiBDD;
    private final String cleanBDD;
    private final String index;

    /**
     * Constructeur
     *
     * @param wikiBDD Chemin vers la base de données wikipédia
     * @param cleanBDD Chemin où stocker la base de données nettoyée
     * @param index Chemin vers le répertoire où stocker l'index Lucéne
     */
    public SearchEngine(String wikiBDD, String cleanBDD, String index) {
        this.wikiBDD = wikiBDD;
        this.cleanBDD = cleanBDD;
        this.index = index;
    }

    /**
     * Crée la base de données propre à partir de la base de données Wikipédia
     *
     * @throws SAXException SAXException
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws IOException IOException
     */
    public void wikiparse() throws SAXException, ParserConfigurationException, IOException {
        System.out.println("launch wikiparse...");
        WikiParseur.parseur(wikiBDD, cleanBDD);
    }

    /**
     * Crée l'index Lucéne à partir de la base de données propre
     *
     * @throws ParserConfigurationException ParserConfigurationException
     * @throws SAXException SAXException
     * @throws IOException IOException
     */
    public void indexer() throws ParserConfigurationException, SAXException, IOException {
        Indexer indexer = new Indexer(index, cleanBDD);
        indexer.create();
    }

    /**
     * Lance un recherche dans l'index Lucéne
     *
     * @param query Chaîne de caractères à rechercher
     * @return Liste des nom de page correspondant à la réponse
     * @throws ParseException ParseException
     * @throws IOException IOException
     * @throws java.text.ParseException java.text.ParseException
     */
    public List<String> search(String query) throws ParseException, IOException, java.text.ParseException {
        Searcher searcher = new Searcher(index);
        return searcher.search(query);
    }

}
